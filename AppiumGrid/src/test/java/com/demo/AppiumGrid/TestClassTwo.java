package com.demo.AppiumGrid;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class TestClassTwo {

	AndroidDriver<MobileElement> driver;
	static DesiredCapabilities cap = new DesiredCapabilities();


	@BeforeTest()
	@Parameters({"deviceID","port","platform_version"})
	public void setUp(String device,String port, String platform_version) throws MalformedURLException{
		File file = new File("src/test/resources/apk/selendroid-test-app-0.17.0.apk");

		cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME , "Android");
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platform_version);
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
		cap.setCapability(MobileCapabilityType.APP, file);
		cap.setCapability("appPackage", "io.selendroid.testapp");
		cap.setCapability("appActivity", "io.selendroid.testapp.HomeScreenActivity");

		URL url = new URL("http://localhost:4444/wd/hub");

		driver = new AndroidDriver<MobileElement>(url, cap);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}


	@Test(priority =1)
	public void testA() throws InterruptedException{
		driver.findElementById("io.selendroid.testapp:id/my_text_field").clear();
		driver.findElementById("io.selendroid.testapp:id/my_text_field").sendKeys("Hello World!");
		System.out.println("ThreadName: " + Thread.currentThread().getName() + Thread.currentThread().getStackTrace()[1].getClassName()+" running on id "+Thread.currentThread().getId());
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp);
		Thread.sleep(3000);

	}

	@Test(priority =2)
	public void testD() throws InterruptedException{
		driver.findElementById("io.selendroid.testapp:id/my_text_field").clear();
		driver.findElementById("io.selendroid.testapp:id/my_text_field").sendKeys("Hello World!");
		System.out.println("ThreadName: " + Thread.currentThread().getName() + Thread.currentThread().getStackTrace()[1].getClassName()+" running on id "+Thread.currentThread().getId());
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp);
		Thread.sleep(2000);

	}
	@Test(priority =3)
	public void testB() throws InterruptedException{
		driver.findElementById("io.selendroid.testapp:id/my_text_field").clear();
		driver.findElementById("io.selendroid.testapp:id/my_text_field").sendKeys("Hello World!");
		System.out.println("ThreadName: " + Thread.currentThread().getName() + Thread.currentThread().getStackTrace()[1].getClassName()+" running on id "+Thread.currentThread().getId());
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp);
		Thread.sleep(2000);

	}
	@Test(priority =4)
	public void testC() throws InterruptedException{
		driver.findElementById("io.selendroid.testapp:id/my_text_field").clear();
		driver.findElementById("io.selendroid.testapp:id/my_text_field").sendKeys("Hello World!");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp);
		System.out.println("ThreadName: " + Thread.currentThread().getName() + Thread.currentThread().getStackTrace()[1].getClassName()+" running on id "+Thread.currentThread().getId());
		Thread.sleep(2000);

	}


	@AfterTest
	public void tearDown(){
		driver.quit();
	}

}
